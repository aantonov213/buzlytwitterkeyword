# BuzlyTwitterKeyword

This app helps users searching in Twitter.

## Compatibility

The app is compatible with iPhones and iPads running on iOS 11.0 and up.
It uses [`Twitter API for Authentication`](https://developer.twitter.com/en/docs/basics/authentication/guides/access-tokens) and [`Standard search API`](https://developer.twitter.com/en/docs/tweets/search/api-reference/get-search-tweets.html) which provides functionality for searching in Twitter.

## Project

The application has only one view which contains search text field and table view for the results.

When the app is started it always authenticates through the [`authenticate`](x-source-tag://AuthenticateMethod) method. Once authenticated the user is able to search which is implemented in [`search(withText: String)`](x-source-tag://SearchMethod) method.

In the Twitter folder there are a few helper files and classes:
- [`TwitterHelper`](x-source-tag://TwitterHelper) has the needed Consumer Key and Consumer Secret for the API calls. It also includes 2 method which help retrieving the URL objects for authentication and searching with the Twitter API.
- [`Tweet`](x-source-tag://Tweet) is the class will contain the needed fields for each tweet. Such as - user name, text of the tweet, etc.
- [`TweetCell`](x-source-tag://TweetCell) is just the cell used in the table that shows the found statuses.

In the Extend folder there are 2 files which add functionality to [`String`](x-source-tag://StringExtension) and [`Date`](x-source-tag://DateExtension) objects.

## User Interface

The UI is in just one file - **Main.storyboard**.
It uses AutoLayout and provides the ability to look well on different screen sizes.
The main view in the Storyboard is connected with `ViewController` and the custom cell is linked with `TweetCell`.
