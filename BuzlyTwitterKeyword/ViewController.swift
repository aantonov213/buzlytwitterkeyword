////
//  ViewController.swift
//  BuzlyTwitterKeyword
//
//  Created by iHustle on 4.05.18.
//  Copyright © 2018 iHome. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    var token: String = ""
    var arrTweets: [Tweet] = []
    
    @IBOutlet var tblTweets: UITableView!
    @IBOutlet var txtSearch: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        tblTweets.delegate = self
        tblTweets.dataSource = self
        tblTweets.estimatedRowHeight = 80.0
        tblTweets.rowHeight = UITableViewAutomaticDimension
        
        self.authenticate()
    }
    
    // MARK: - Twitter API Calls
    
    /**
     Used for [Application-only authentication](https://developer.twitter.com/en/docs/basics/authentication/overview/application-only)
     */
    /// - Tag: AuthenticateMethod
    func authenticate() {
        // Setup the URL Request
        var request = URLRequest(url: Twitter.URLs.URL_OAuth2_Token)
        request.httpMethod = "POST"
        
        // Prepare the header
        let contentType = "application/x-www-form-urlencoded;charset=UTF-8"
        let credentials = "\(Twitter.AppSettings.ConsumerKey):\(Twitter.AppSettings.ConsumerSecret)"
        
        let header = ["Authorization": "Basic \(credentials.data(using: .utf8)!.base64EncodedString())", "Content-Type": contentType]
        
        request.allHTTPHeaderFields = header
        
        // Prepare the body
        let body = "grant_type=client_credentials"
        request.httpBody = body.data(using: .utf8)!
        
        // Make the request
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, resp, error) in
            do {
                if let result = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:String] {
                    if error != nil {
                        self.showAlert(withMessage: "There was a problem with the app. Please try again later!")
                    } else {
                        if let access_token = result["access_token"] {
                            self.token = access_token
                        }
                    }
                }
            }
            catch {
                print(error.localizedDescription)
            }
        }
        
        task.resume()
    }

    /**
     Searches in Twitter status by text.
     
      - Parameter withText: search text that will be used
      - Parameter page: number of page to load
    */
    /// - Tag: SearchMethod
    func search(withText: String) {
        // Setup the URL Request
        var request = URLRequest(url: Twitter.URLs.URL_Search(query: withText))
        request.httpMethod = "GET"
        
        // Prepare the header
        let contentType = "application/x-www-form-urlencoded;charset=UTF-8"
        let header = ["Authorization": "Bearer \(self.token)", "Content-Type": contentType]
        
        request.allHTTPHeaderFields = header
        
        // Make the request
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, resp, error) in
            do {
                if let result = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any] {
                    if error != nil {
                        self.showAlert(withMessage: "There was a problem. Please try again!")
                    } else {
                        if let statuses = result["statuses"] as? [[String: Any]] {
                            self.arrTweets.removeAll()
                            
                            for status in statuses {
                                self.arrTweets.append(Tweet(dictTweet: status))
                            }
                            DispatchQueue.main.async {
                                self.tblTweets.reloadData()
                            }
                        }
                    }
                }
            }
            catch {
                print(error.localizedDescription)
            }
        }
        
        task.resume()
    }
    
    // MARK: - UITableView
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TweetCell")! as! TweetCell
        let tweet = arrTweets[indexPath.row]
        
        cell.update(tweet: tweet)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTweets.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tweet = self.arrTweets[indexPath.row]
        tweet.openTweet()
    }
    
    // MARK: - UITextView
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        self.search(withText: textField.text!)
        self.tblTweets.contentOffset.y = 0.0
        
        return true
    }
    
    // MARK: - UIScrollView
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // If scrolling while the keyboard is shown - hide the keyboard.
        if scrollView == self.tblTweets {
            if self.txtSearch.isFirstResponder {
                self.txtSearch.resignFirstResponder()
            }
        }
    }
    
    // MARK: - Other
    
    func showAlert(withMessage: String) {
        let alert = UIAlertController(title: "Oops", message: withMessage, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

