//
//  String+Extensions.swift
//  BuzlyTwitterKeyword
//
//  Created by iHustle on 5.05.18.
//  Copyright © 2018 iHome. All rights reserved.
//

import Foundation

extension String {
    /// - Tag: StringExtension
    func toDate(format: String) -> Date? {
        let df = DateFormatter()
        df.dateFormat = format
        
        return df.date(from: self)
    }
}
