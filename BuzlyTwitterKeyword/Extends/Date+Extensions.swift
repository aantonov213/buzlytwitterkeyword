//
//  Date+Extensions.swift
//  BuzlyTwitterKeyword
//
//  Created by iHustle on 5.05.18.
//  Copyright © 2018 iHome. All rights reserved.
//

import Foundation

extension Date {
    /// - Tag: DateExtension
    func toString(format: String) -> String {
        let df = DateFormatter()
        df.dateFormat = format
        
        return df.string(from: self)
    }
}
