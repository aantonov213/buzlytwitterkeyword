//
//  TweetCell.swift
//  BuzlyTwitterKeyword
//
//  Created by iHustle on 5.05.18.
//  Copyright © 2018 iHome. All rights reserved.
//

import UIKit

/// - Tag: TweetCell
class TweetCell: UITableViewCell {

    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblTweet: UILabel!
    
    /**
     Updates the cell with the new __Tweet__ information
    */
    func update(tweet: Tweet) {
        self.lblDate.text = tweet.createdAt != nil ? tweet.createdAt!.toString(format: "dd-MM-yyyy") : ""
        self.lblUserName.text = tweet.userName != nil ? "@\(tweet.userName!)" : ""
        self.lblTweet.text = tweet.text != nil ? tweet.text! : ""
        self.imgProfile.image = nil
        
        // Make sure the status text is fully shown
        self.lblTweet.lineBreakMode = NSLineBreakMode.byWordWrapping
        self.lblTweet.sizeToFit()
        
        // Load the image in the backgruond
        if let profileImageUrl = tweet.profileImageUrl {
            DispatchQueue.global().async {
                do {
                    let dataImage = try Data(contentsOf: profileImageUrl)
                    
                    // Update the image from the main queue
                    DispatchQueue.main.async {
                        if profileImageUrl.absoluteString == tweet.profileImageUrl!.absoluteString {
                            self.imgProfile.image = UIImage(data: dataImage)
                        }
                    }
                }
                catch {
                    DispatchQueue.main.async {
                        self.imgProfile.image = nil
                    }
                }
            }
        }
    }
}
