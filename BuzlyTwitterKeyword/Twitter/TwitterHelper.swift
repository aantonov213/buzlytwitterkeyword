//
//  TwitterHelper.swift
//  BuzlyTwitterKeyword
//
//  Created by iHustle on 4.05.18.
//  Copyright © 2018 iHome. All rights reserved.
//

import Foundation

/// - Tag: TwitterHelper
struct Twitter {
    struct AppSettings {
        static let ConsumerKey = "SHyaArOT0c4N7EiGAZJDlasYS"
        static let ConsumerSecret = "SVpV5T6Uvf9a0nFARwJnl4nS5SwxCQsxlWl003PcN5rQBMUIWk"
    }
    
    struct URLs {
        static let Domain = "api.twitter.com"
        static let OAuth2_Token = "/oauth2/token"
        static let Search = "/1.1/search/tweets.json?q="
        
        /**
         - Returns: URL object for OAuth2_Token in Twitter
        */
        static var URL_OAuth2_Token: URL {
            return URL(string: "https://\(Twitter.URLs.Domain)\(Twitter.URLs.OAuth2_Token)")!
        }
        
        /**
         Setup for getting an URL object with correct url for searching in twitter
         
         - Returns: URL object for searching in Twitter
         
         - Parameter query: string that will be used in the search process
         */
        static func URL_Search(query: String) -> URL {
            return URL(string: "https://\(Twitter.URLs.Domain)\(Twitter.URLs.Search)\(query.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)&count=100")!
        }
    }
}
