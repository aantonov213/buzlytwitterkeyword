//
//  Tweet.swift
//  BuzlyTwitterKeyword
//
//  Created by iHustle on 5.05.18.
//  Copyright © 2018 iHome. All rights reserved.
//

import UIKit

/// - Tag: Tweet
class Tweet: NSObject {
    var createdAt: Date?
    var userName: String?
    var profileImageUrl: URL?
    var text: String?
    var statusId: String?
    
    /**
     Initialize a Tweet from dictionary
    */
    init(dictTweet: [String: Any]) {
        if let created_at = dictTweet["created_at"] as? String {
            self.createdAt = created_at.toDate(format: "eee MMM dd HH:mm:ss ZZZZ yyyy")
        }
        
        if let user = dictTweet["user"] as? [String: Any] {
            if let user_name = user["screen_name"] as? String {
                self.userName = user_name
            }
            
            if let profile_image = user["profile_image_url"] as? String {
                self.profileImageUrl = URL(string: profile_image)
            }
        }
        
        if let tweet_text = dictTweet["text"] as? String {
            self.text = tweet_text
        }
        
        if let id_string = dictTweet["id_str"] as? String {
            self.statusId = id_string
            //self.tweetUrl = URL(string: "https://twitter.com/i/web/status/\(id_string)")!
        }
    }
    
    /**
     Opens the status in Twitter app if available. Otherwise would open it in Safari.
    */
    func openTweet() {
        if let status_id = statusId {
            if UIApplication.shared.canOpenURL(URL(string: "twitter://")!) {
                if let tweetAppUrl = URL(string: "twitter://status?id=\(status_id)") {
                    UIApplication.shared.open(tweetAppUrl, options: [:]) { (b) in
                        
                    }
                }
            }
            else if let tweetUrl = URL(string: "https://twitter.com/i/web/status/\(status_id)") {
                UIApplication.shared.open(tweetUrl, options: [:]) { (b) in
                    
                }
            }
        }
    }
    
    override var description: String {
        return "\(self.userName == nil ? "<no name>" : self.userName!) tweeted \(self.text == nil ? "no text" : self.text!) @ \(self.createdAt == nil ? "<no date>" : self.createdAt!.toString(format: "dd-MM-yyyy"))"
    }
}






